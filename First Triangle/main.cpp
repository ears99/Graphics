#include <cstdlib>
#include <iostream>
#include <GL/glew.h>
#include <GL/gl.h>
#include <SDL2/SDL.h>
using namespace std;

//GLOBAL VARIABLES

bool init_resources(void) {
  
  return true;
}

void render(SDL_Window* window) {
  
}

void free_resources() {

}

void mainLoop(SDL_Window* window) {
	while (true) {
		SDL_Event event;
		while (SDL_PollEvent(&event)) {
			switch(event.type) {
				case SDL_QUIT:
					SDL_Quit();
					break;
			}
		}
		render(window);
	}
}

int main(int argc, char* argv[]) {
	SDL_Init(SDL_INIT_VIDEO);
	SDL_Window* window = SDL_CreateWindow("My First Triangle", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, 600, 400, SDL_WINDOW_OPENGL);
	SDL_GL_CreateContext(window);

	
	GLenum glew_status = glewInit();
	if (glew_status != GLEW_OK) {
		cerr << "Error: glewInit: " << glewGetErrorString(glew_status) << endl;
		return EXIT_FAILURE;
	}

	if (!init_resources())
		return EXIT_FAILURE;

	//Display something if everything goes OK
	mainLoop(window);

	//If the program exits in the usual way, free resources and exit with a success
	free_resources();
	return EXIT_SUCCESS;
}
