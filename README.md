# Graphics
A repository for all my graphics programming, using SDL2, OpenGL 2.1, and C++

# Why OpenGL 2.1? That's outdated!
The reason I'm using OpenGL is because, long story short, the graphics card (and by extension my computer) is old. Either way, OpenGL 2.1 is still considered modern OpenGL.
